.. |label| replace:: 1st Vision Konfigurierbare Adressenfelder
.. |snippet| replace:: FvCustomAddress
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.2.0
.. |maxVersion| replace:: 5.5.7
.. |version| replace:: 1.0.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Mit diesem Plugin haben Sie die Möglichkeit die Eingabe in den Adressfeldern zu beschränken. ntsprechend der im Backend hinterlegten Konfiguration bzgl. der Länge zu beschränken.

Frontend
--------
Sowohl bei der Neu-Registrierung als auch beim Abändern der bestehenden Adresssätze werden die Felder entsprechend der im Backend hinterlegten Konfiguration bzgl. der Länge beschränkt.

Backend
-------
Pro Adressfeld (nur Texteingabe) kann ein Wert eingetragen werden, der für die erlaubte Maximallänge steht. Bei fehlendem Eintrag oder 0 bleibt das Feld unbeschränkt.

.. image:: FvCustomAddress1.png

Textbausteine
_____________

keine

technische Beschreibung
------------------------
Die Zeichenbegrenzung erfolgt über das HTML-Attribut maxlength

Modifizierte Template-Dateien
-----------------------------
:/account/profile.tpl:
:/address/form.tpl:
:/register/billing_fieldset.tpl:
:/register/shipping_fieldset.tpl:
:/register/personal_fieldset.tpl:


